﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class EmissionFlicker : MonoBehaviour {
    
    public new Renderer renderer;
    public Texture[] textures;
    private int activeTexture;
    public float MinTime;
    public float MaxTime = 1f;

	// Use this for initialization
	void Start () {
        renderer = GetComponent<Renderer>();

        activeTexture = -1;
        StartCoroutine(SetTexture());
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    private IEnumerator Countdown()
    {
        yield return new WaitForSeconds(Random.Range(MinTime, MaxTime));
        StartCoroutine(SetTexture());
    }

    private IEnumerator SetTexture()
    {
        int old = activeTexture;
        do
        {
            activeTexture = Random.Range(0, textures.Length);
            if (activeTexture == old)
                yield return null;
        } while (old == activeTexture);

        renderer.material.SetTexture("_EmissionMap", textures[activeTexture]);
        StartCoroutine(Countdown());
    }

}
