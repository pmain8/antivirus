﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Powerups { Truesight, Invisible, Gun, Tunnel, BugAttractor, AdBlocker, Firewall, Decryptor, MAX }

public class Player : Ship
{
    // Fields =====================================================================================
    #region Fields
    protected bool[] bPowerups;
    protected float[] fPowerupsLife;
    protected int bullets;
    private const float POWERUP_MAX_LIFE = 5.0f;
    private const int MAX_BULLETS = 10;
    private bool hasShot;
    #endregion
    // ============================================================================================

    // MonoBehavior ===============================================================================
    #region Mono
    public override void Start ()
    {
        base.Start();
        doUpdate = true;
        bPowerups = new bool[(int)Powerups.MAX];
        fPowerupsLife = new float[(int)Powerups.MAX];
    }
    // ----------------------------------------------------------------------------------
    public override void Update ()
    {
        base.Update();
        if (doUpdate)
        {
            if (Input.GetKey(KeyCode.Space))
                speed = 0;
            else
                speed = maxSpeed;
		}
		
        if (bPowerups[(int)Powerups.Gun])
        {
            if (bullets <= 0)
                bPowerups[(int)Powerups.Gun] = false;
            else if (Input.GetAxis("RightTrigger") > 0 && !hasShot)
                ShootBullet();
            else if (Input.GetAxis("RightTrigger") <= 0 && hasShot)
                hasShot = false;
        }

        for(int i = 0; i < (int)Powerups.MAX; i++)
        {
            if (i != (int)Powerups.Gun && bPowerups[i])
            {
                if(fPowerupsLife[i] > 0.0f)
                    fPowerupsLife[i] -= Time.deltaTime;
                else
                {
                    fPowerupsLife[i] = 0.0f;
                    bPowerups[i] = false;
                }
            }
        }
    }
    // ----------------------------------------------------------------------------------
    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }
    #endregion
    // ============================================================================================

    // Collision ==================================================================================
    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag.Equals("Shattering"))
        {
            if (collision.collider.GetComponent<Ship>())
                collision.collider.GetComponent<Ship>().ReceiveDamage();
        }
        else if (collision.collider.gameObject.tag.Equals("Damaging"))
            base.OnCollisionEnter(collision);
        else // tag is Bouncing or untagged
            base.OnCollisionEnter(collision);


    }
    // ============================================================================================

    // Movement ===================================================================================
    public override float ChooseDirection()
    {
        return Input.GetAxis("Horizontal");
    }
    public override void ReceiveDamage()
    {
        // Do something...
    }
    // ============================================================================================

    // Accessors ==================================================================================
    public int GetBullets() { return bullets; }
    public bool IsPowerupAvailable(Powerups p)
    {
        return bPowerups[(int)p];
    }
    public void GainPowerup(Powerups p)
    {
        bPowerups[(int)p] = true;
        fPowerupsLife[(int)p] = POWERUP_MAX_LIFE;
        if (p == Powerups.Gun)
            bullets = MAX_BULLETS;
    }
    public void ShootBullet()
    {
        bullets -= 1;
        hasShot = true;
        if (bullets <= 0)
            bPowerups[(int)Powerups.Gun] = false;
    }
    // ============================================================================================
}
