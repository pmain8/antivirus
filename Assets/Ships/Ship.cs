﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class Ship : MonoBehaviour
{
    // Fields =====================================================================================
    #region Fields
    // Components -------------------------------
    protected Rigidbody rb;
    protected bool doUpdate;
    //private Collider col;

    // Movement ---------------------------------
    //private Vector3 newPosition;
    protected float speed;
    public float maxSpeed = 0.3f;
    private float turnAmount;
    public float rateOfTurn = 3f;
    public float WallTurnRate = 0.2f;
    private Vector3 turnConstraints;

    #endregion
    // ============================================================================================

    // MonoBehavior ===============================================================================
    #region Mono
    public virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        //col = GetComponent<Collider>();
        //newPosition = transform.position;
        RigidbodyConstraints rc = rb.constraints;
        turnConstraints = new Vector3(
            (rc & RigidbodyConstraints.FreezeRotationX).Equals(RigidbodyConstraints.FreezeRotationX) ? 0 : 1,
            (rc & RigidbodyConstraints.FreezeRotationY).Equals(RigidbodyConstraints.FreezeRotationY) ? 0 : 1,
            (rc & RigidbodyConstraints.FreezeRotationZ).Equals(RigidbodyConstraints.FreezeRotationZ) ? 0 : 1
            );
        speed = maxSpeed;
    }
    // ----------------------------------------------------------------------------------
    public virtual void Update()
    {
        turnAmount = ChooseDirection();
    }
    // ----------------------------------------------------------------------------------
    public virtual void FixedUpdate()
    {
        if (!doUpdate) return;

        rb.MoveRotation(Quaternion.AngleAxis(transform.rotation.eulerAngles.y + turnAmount * rateOfTurn, turnConstraints));
        rb.MovePosition(transform.position + speed * transform.forward);
    }
    #endregion
    // ============================================================================================

    // Collision ==================================================================================
    public virtual void OnCollisionEnter(Collision collision)
    {
        speed = 0;
        rb.angularVelocity = Vector3.zero;
        doUpdate = false;
        //transform.rotation = transform.rotation * Quaternion.FromToRotation(transform.forward, Vector3.Reflect(transform.forward, collision.contacts[0].normal));
        StartCoroutine(Rot(Vector3.Reflect(transform.forward, collision.contacts[0].normal)));
        //Debug.Log("boom");

    }
    private IEnumerator Rot(Vector3 v)
    {
        Quaternion q = transform.rotation * Quaternion.FromToRotation(transform.forward, v);
        while (Mathf.Abs(Vector3.Angle(transform.forward, v)) > 0.5)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, q, WallTurnRate);
            yield return null;
        }

        //Debug.Log("Fin");

        speed = maxSpeed;
        doUpdate = true;
    }
    // ============================================================================================

    // Movement ===================================================================================
    // Decide where to turn frame -------------------------------------------------------
    public abstract float ChooseDirection();

    // ============================================================================================
}
