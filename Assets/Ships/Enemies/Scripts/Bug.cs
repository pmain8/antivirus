﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bug : Ship
{
    // Fields =====================================================================================
    #region Fields
    #endregion
    // ============================================================================================

    // MonoBehavior ===============================================================================
    #region Mono
    public override void Start()
    {
        base.Start();
        doUpdate = true;
    }
    // ----------------------------------------------------------------------------------
    public override void Update()
    {
        base.Update();
    }
    // ----------------------------------------------------------------------------------
    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }
    #endregion
    // ============================================================================================

    // Movement ===================================================================================
    public override float ChooseDirection()
    {
        if (doUpdate)
        {
            float magn = Random.Range(1, 5);
            float sign = Mathf.Sign(Random.Range(-1, 1));
            rb.MovePosition(new Vector3(
                magn * sign,
                0, 0
                ));
        }
        return Random.Range(-10, 10);
    }
    // Damage this ship ----------------------------------------------------------------- 
    public override void ReceiveDamage()
    {
        Instantiate(this, Vector3.zero, transform.rotation);
        Destroy(this.gameObject);
    }
    // ============================================================================================
}
